#include "tictoe.h"
#include "iostream"
#include <conio.h>
using namespace std;

int TicTacToe::wynik(pole tab[3][3],bool pr )
{
    pole sign;
    if(pr)
    {
        sign=KOLKO;
    }
    else
    {
        sign=KRZYZYK;
    }
    if((tab[0][0]==sign&&tab[0][1]==sign&&tab[0][2]==sign)||
       (tab[1][0]==sign&&tab[1][1]==sign&&tab[1][2]==sign)||
       (tab[2][0]==sign&&tab[2][1]==sign&&tab[2][2]==sign)||
       (tab[0][0]==sign&&tab[1][0]==sign&&tab[2][0]==sign)||
       (tab[0][1]==sign&&tab[1][1]==sign&&tab[2][1]==sign)||
       (tab[0][2]==sign&&tab[1][2]==sign&&tab[2][2]==sign)||
       (tab[0][0]==sign&&tab[1][1]==sign&&tab[2][2]==sign)||
       (tab[0][2]==sign&&tab[1][1]==sign&&tab[2][0]==sign))
       {
           if(pr==_kolko)
           {
               return 1;
           }
           else
           {
               return -1;
           }
       }
       else if(tab[0][0]!=PUSTE&&tab[0][1]!=PUSTE&&tab[0][2]!=PUSTE&&
        tab[1][0]!=PUSTE&&tab[1][1]!=PUSTE&&tab[1][2]!=PUSTE&&
        tab[2][0]!=PUSTE&&tab[2][1]!=PUSTE&&tab[2][2]!=PUSTE)
        {
            return 0;
        }
        else return 2;




}
int TicTacToe::int_sprawdzanko(pole tab[3][3],int n, bool pr)
{
    pole pom[3][3];
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<3;j++)
        {
            if((3*i+j)==n)
            {
                if(pr)
                {
                   pom[i][j]=KOLKO;
                }
                else
                {
                    pom[i][j]=KRZYZYK;
                }
            }
            else
            {
                pom[i][j]=tab[i][j];
            }
        }
    }
    if(wynik(pom,pr)==1)
    {
        return 1;
    }
    else if(wynik(pom,pr)==-1)
    {
        return -1;
    }
    else if(wynik(pom,pr)==0)
    {
        return 0;
    }
    else
    {
        int suma=0;
        for (int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                if(pom[i][j]==PUSTE)
                {
                    suma= suma+ int_sprawdzanko(pom,(3*i+j),!pr);
                }
            }
        }
        return suma;
    }
    return 0;


}
void TicTacToe::minimax()
{
    int pom[9];
    int index=0;
    for(int i=0;i<9;i++)
    {
        if(kontrola(i))
        {
            pom[i]=int_sprawdzanko(_pole,i,_kolko);
        }
        else
        {
            pom[i]=0;
        }
        if(pom[i]>pom[index])
        {
            index=i;
        }
    }
    //ustawia pod danym indeksem dany znak
    //int index=7;
    /*cout<<index<<endl;
    cout<<_pole[0][0]<<endl;
    _pole[0][0]=KOLKO;
    cout<<_pole[0][0]<<endl;
    cout<<"dziala";*/
    _pole[index/3][index%3]=this->_znak;
    if(this->_znak==KRZYZYK)
    {
       _numery[index/3][index%3] ='X';
    }
    else
    {
        _numery[index/3][index%3] ='O';
    }

    getch();


}
void TicTacToe::set_gracz(bool a)
{
    _gracz=a;
}
void TicTacToe::set_kolko(bool a)
{
    _kolko=a;
}
bool TicTacToe::get_gracz()
{
    return _gracz;
}
bool TicTacToe::get_kolko()
{
    return _kolko;
}
