#include "tictoe.h"
#include "iostream"
#include <conio.h>
#include <windows.h>
using namespace std;

pole TicTacToe::_pole[3][3]={{PUSTE,PUSTE,PUSTE},{PUSTE,PUSTE,PUSTE},{PUSTE,PUSTE,PUSTE}};
char TicTacToe::_numery[3][3]={{'1','2','3'},{'4','5','6'},{'7','8','9'}};

TicTacToe::TicTacToe(pole z)
{
    _znak=z;

}

bool TicTacToe::sprawdzanko()
{



    if((_pole[0][0]==_znak&&_pole[0][1]==_znak&&_pole[0][2]==_znak)||
       (_pole[1][0]==_znak&&_pole[1][1]==_znak&&_pole[1][2]==_znak)||
       (_pole[2][0]==_znak&&_pole[2][1]==_znak&&_pole[2][2]==_znak)||
       (_pole[0][0]==_znak&&_pole[1][0]==_znak&&_pole[2][0]==_znak)||
       (_pole[0][1]==_znak&&_pole[1][1]==_znak&&_pole[2][1]==_znak)||
       (_pole[0][2]==_znak&&_pole[1][2]==_znak&&_pole[2][2]==_znak)||
       (_pole[0][0]==_znak&&_pole[1][1]==_znak&&_pole[2][2]==_znak)||
       (_pole[0][2]==_znak&&_pole[1][1]==_znak&&_pole[2][0]==_znak))
    {
        if(_znak==KRZYZYK)
    {

        cout<<endl<<"Wygral krzyzyk!"<<endl;
    }
    else
    {

        cout<<endl<<"Wygralo kolko!"<<endl;
    }
        return false;
    }
    else if(_pole[0][0]!=PUSTE&&_pole[0][1]!=PUSTE&&_pole[0][2]!=PUSTE&&
            _pole[1][0]!=PUSTE&&_pole[1][1]!=PUSTE&&_pole[1][2]!=PUSTE&&
            _pole[2][0]!=PUSTE&&_pole[2][1]!=PUSTE&&_pole[2][2]!=PUSTE)
    {

        cout<<endl<<endl<<"Remis!";
        return false;
    }
    return true;

}
void TicTacToe::wypisz_wiersz(int ktory)
{
    cout<<"| ";
    for(int i=0;i<3;i++)
    {

            cout<<_numery[ktory][i];
        cout<<" | ";
    }
}

void TicTacToe::wypisz()
{
    if(_znak==KRZYZYK)
    {
        cout<<"Ruch krzyzyka!"<<endl;
    }
    else
    {
        cout<<"Ruch kolka!"<<endl;
    }
     cout<<"-------------"<<endl;
    wypisz_wiersz(0);
    cout<<endl;
    cout<<"-------------"<<endl;
     wypisz_wiersz(1);
    cout<<endl;
    cout<<"-------------"<<endl;
     wypisz_wiersz(2);
    cout<<endl;
    cout<<"-------------"<<endl;
}
bool TicTacToe::kontrola(int numer)
{
    if((numer<0)||(numer>8))
    {
        cout<<endl<<"Bledny numer - powtorz"<<endl;
        return false;
    }


    if((KRZYZYK==_pole[numer/3][numer%3])||(KOLKO==_pole[numer/3][numer%3])) return false;
    else return true;



}
void TicTacToe::wpisywanie()
{

    int licz;
    bool tym = false;
    while(!tym){
            cout<<endl<<"Wpisz numer pola: ";
    while(!(cin>>licz))
        {
            cout<<"Blad - prosze podac cyfre! ";
            cin.clear();
            cin.ignore(9999, '\n' );

        }
        licz--;
        tym=kontrola(licz);

    }
    _pole[licz/3][licz%3]=_znak;
    //cout<<endl<<licz/3<<" "<<licz%3;
    //getch();
    if(_znak==KRZYZYK)
    {
       _numery[licz/3][licz%3] ='X';
    }
    else
    {
        _numery[licz/3][licz%3] ='O';
    }
}
pole TicTacToe::get_znak()
{
    return this->_znak;
}
