#ifndef TICTOE_H_INCLUDED
#define TICTOE_H_INCLUDED
enum pole {PUSTE,KOLKO,KRZYZYK};
class TicTacToe
{
public:
    TicTacToe(pole);
    void wypisz();
    bool sprawdzanko();
    void wpisywanie();
    void wypisz_wiersz(int);
    bool kontrola(int);
    int wynik(pole[3][3],bool);
    int int_sprawdzanko(pole[3][3],int,bool);
    void minimax();
    void set_gracz(bool);
    void set_kolko(bool);
    bool get_gracz();
    bool get_kolko();
    pole get_znak();




private:
    pole _znak;
    static pole _pole[3][3];
    static char _numery[3][3];
    static bool _kolko, _gracz;



};


#endif // TICTOE_H_INCLUDED
