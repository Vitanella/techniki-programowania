#define _WIN32_WINNT 0x0500
#include <windows.h>
#include <string>
#include <iostream>

using namespace std;
CONSOLE_SCREEN_BUFFER_INFO csbiInfo;
HANDLE handelek = GetStdHandle(STD_OUTPUT_HANDLE);
void kolor(int tlo, int kol_napis, int intensywnosnosc)
{
    SetConsoleTextAttribute(handelek,(tlo*16+kol_napis+8*intensywnosnosc));
}

void polozenie(short x, short y)
{
    COORD coord = { x, y };
    SetConsoleCursorPosition(handelek,coord);
}
int roz_ekr (int co)
{
    GetConsoleScreenBufferInfo(handelek,&csbiInfo);
    if(co==0)
    return csbiInfo.dwSize.X;
    else if(co==1)
    return csbiInfo.dwSize.Y;
    else
    return 0;
}
void srodek(string napis, int linijka, int czas)
{
    int ilosc=napis.length();
    polozenie((roz_ekr(0)-ilosc)/2,linijka);
    for(int i =0;i<ilosc;i++)
    {
        cout<<napis[i];
        Sleep(czas );
    }
}
void ustawienia_okna (short x, short y)
 {
     SMALL_RECT windowSize = { 0, 0, x, y }; //deklaracja wielkosci okna
     HWND uchwytKonsoli = GetConsoleWindow();
     MoveWindow( uchwytKonsoli, 0, 0, 5000, 5000, TRUE );
     SetConsoleWindowInfo( handelek, 1, & windowSize );


 }
void ramka(int tlo_kol, short x, short y, int szer, int dlu, bool wyp)
{
    kolor(tlo_kol,0,0);
    polozenie(x,y);
    for(int i=0;i<=szer;i++) cout<<" ";
    polozenie(x,y+dlu);
    for(int i=0;i<=szer;i++) cout<<" ";
    for(int i=0;i<=dlu;i++)
    {
        polozenie(x,y+i);
        cout<<" ";
    }
    for(int i=0;i<=dlu;i++)
    {
        polozenie(x+szer,y+i);
        cout<<" ";
    }
    if(wyp)
    {
        for(int i=0;i<=(dlu-2);i++)
        {
            polozenie(x+1,y+1+i);
            for(int j=0;j<=(szer-2);j++)
                {
                    cout<<" ";
                }
        }
    }
}
