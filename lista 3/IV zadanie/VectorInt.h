#ifndef VECTORINT_H_INCLUDED
#define VECTORINT_H_INCLUDED
#include <iostream>

 class VectorInt
 {
 public:
    VectorInt(); //konstruktor pusty
    VectorInt(int);  //konstruktor z wartoscia
    VectorInt(const VectorInt&);  //konstruktor kopiujacy
    VectorInt& operator= (const VectorInt& );  //zdefiniowanie operatora przypisania dla obiektow naszej klasy
    ~VectorInt(); //destruktor
    int at(int);  //wypisuje dany element wektora
    void insert(int, int);  //wpisuje na dane miejsce we wektorze dana wartosc
    void pushback(int);  //dopisuje dana wartosc na koncu wektora, tj. [1,2,5].pushback(8)=[1,2,5,8]
    void popback();   //usuwa ostatni element wektora, tj. [1,2,5].popback()=[1,2,]
    void shrinkToFit();  //redukuje zaalokowona pamiec tylko do rozmiaru wektora
    void clear();  //czysci (zeruje) wszystkie elementy wektora
    int size();    //zwraca rozmiar wektora
    int capacity();  //zwraca dostepna pamiec

    //Moje funkcje, ktore ulatwiaja prace:

    int get_dlugosc();  //zwraca zaalokowana pamiec
    void realok(int);  //realokuje pamiec do danej wartosci
    void reg_size();   //reguluje rozmiar wektora

 private:
     int *_wek;  //wskaznik, ktory sluzy za tablice dynamiczna
     int _dlugosc;  //przechowuje zaalokowana pamiec
     int _size;  //przechowuje rozmiar wektora
     int _capacity;  //przechowuje wolne miejsce w pamieci (_capacity=_dlugosc-_size)
 };

std::ostream& operator<<(std::ostream&,VectorInt&); //definicja operatora "<<" dla obiektow naszej klasy

#endif // VECTORINT_H_INCLUDED
