#include <iostream>
#include "VectorInt.h"  //tam gdzie są zdefiniowane instrukcje wektora
#include "sett.h"       //moja prywatna biblioteka z funkcjami do konsolki
#include <conio.h>
#include <windows.h>
using namespace std;

int main()
{
    bool dziala=true;
    bool exist=false;
    VectorInt *wektor;
    char opcje;
    do
    {
        if(!exist)
        {
            kolor(0,6,1);
            srodek("Vektorex - i ty mozesz stworzyc swoj wektor :D",0,50);
            cout<<endl;
            kolor(0,4,1);
            cout<<"Twoj wektor jeszcze nie istnieje!"<<endl<<"Na ile wymiarow zaalokowac pamiec: ";
            int tym;
            while(!(cin>>tym))
            {
                cout<<"Blad - prosze podac liczbe calkowita: ";
                cin.clear();
                cin.ignore(9999, '\n' );
            }
            wektor=new VectorInt(tym);
            system("cls");
            exist=true;
        }
        kolor(0,6,1);
        srodek("Vektorex - i ty mozesz stworzyc swoj wektor :D",0,0);
        kolor(0,7,1);
        cout<<endl<<endl;
        if(wektor->size()==0)
        {
            kolor(0,4,1);
            cout<<"Pusty wektor! Dodaj cos do niego!";
        }
        else
        {
            kolor(0,4,1);
            cout<<"Twoj wektor: "<<(*wektor);
        }
        kolor(0,7,1);
        cout<<endl<<"Zaalokowana pamiec na miejsc: "<<wektor->get_dlugosc();
        cout<<endl<<"Liczba wymiarow: "<<wektor->size();
        cout<<endl<<"Liczba wolnych miejsc: "<<wektor->capacity();

        cout<<endl;
        kolor(0,6,1);
        srodek("Opcje",6,0);
        kolor(0,7,1);
        cout<<endl<<"1. Wyswietl dany element wektora.";
        cout<<endl<<"2. Dopisz wartosc.";
        cout<<endl<<"3. Dodaj kolejny wymiar wektora.";
        cout<<endl<<"4. Usun ostatni wymiar wektora.";
        cout<<endl<<"5. Wyczysc wszystkie wymiary.";
        cout<<endl<<"6. Usun niewykorzystywana pamiec.";
        cout<<endl<<"7. Wyjdz z programu.";
        cout<<endl<<endl<<"Twoj wybor: ";
        cin>>opcje;

        switch(opcje)
        {
        case '1':
            {
                bool tym=false;
                int licz;
                while(!tym){
                cout<<endl<<"Ktory element chcesz zobaczyc? (1-"<<wektor->get_dlugosc()<<")";
                cout<<endl<<"Wpisz numer pola: ";
                while(!(cin>>licz))
                {
                    cout<<"Blad - prosze podac liczbe ca³kowita: ";
                    cin.clear();
                    cin.ignore(9999, '\n' );
                }
                if((licz<=0)||(licz>wektor->get_dlugosc()))
                {
                    cout<<endl<<"Podaj liczbe z zakresu!";
                }
                else
                {
                    tym=true;
                }
                }
                cout<<"v_"<<licz<<" = "<<wektor->at(licz);
                getch();
                break;
            }
        case '2':
            {
                int licz1, licz2;
                cout<<endl<<"Na ktorym miejscu chcesz wstawic wartosc: ";
                while(!(cin>>licz1))
                {
                    cout<<"Blad - prosze podac liczbe ca³kowita: ";
                    cin.clear();
                    cin.ignore(9999, '\n' );
                }
                cout<<endl<<"Jaka wartosc chcesz tam wstawic: ";
                while(!(cin>>licz2))
                {
                    cout<<"Blad - prosze podac liczbe ca³kowita: ";
                    cin.clear();
                    cin.ignore(9999, '\n' );
                }
                wektor->insert(licz1,licz2);
                break;
            }
        case '3':
            {
                int licz;
                cout<<endl<<"Jaka wartosc chcesz wstawic: ";
                while(!(cin>>licz))
                {
                    cout<<"Blad - prosze podac liczbe ca³kowita: ";
                    cin.clear();
                    cin.ignore(9999, '\n' );
                }
                wektor->pushback(licz);
                break;
            }
        case '4':
            {

                wektor->popback();
                break;
            }
        case '5':
            {
                wektor->clear();
                break;
            }
        case '6':
            {
                wektor->shrinkToFit();
                break;
            }
        case '7':
            {
                delete wektor;
                dziala=false;
                break;
            }
        default:
            {
                cout<<endl<<"Wybierz prawidlowa opcje!";
                getch();
                break;
            }
        }
        system("cls");
    }while(dziala);
    return 0;
}
