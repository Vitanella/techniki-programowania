#include "VectorInt.h"
#include <iostream>
#include <string>
#include <sstream>
//komentarze dla potrzebujacych kolegow

VectorInt::VectorInt() //konstruktor pusty
{
    _dlugosc=16;  //ustawia zaalokowana pamiec na 16
    _size=0;   //nie ma nic wiec rozmiar wektora zerowy
    _capacity=_dlugosc;  //cala pamiec jest wolna
    _wek = new int [_dlugosc]; //alokacja pamieci
    for(int i=0;i<_dlugosc;i++)
    {
        _wek[i]=0;  //ustawia wszystkim mozliwym elementom 0 bo jest to zerowy, pusty wektor
    }

}
VectorInt::VectorInt(int n) //jedyna roznica ze tu wpisujemy na ile chcemy zaalokowac
{
    _dlugosc=n;
    _size=0;
    _capacity=_dlugosc;
    _wek = new int [_dlugosc];
    for(int i=0;i<_dlugosc;i++)
    {
        _wek[i]=0;
    }
}
VectorInt::VectorInt(const VectorInt& inny) //konstruktor kopiujacy
{
    this->_dlugosc=inny._dlugosc; //wszystkie zmienne ladnie kopiujemy z jedneko obiektu do drugiego
    this->_size=inny._size;
    this->_capacity=inny._capacity;
    this->_wek=new int [_dlugosc];  //alokujemy pamiec jak w powyzszych konstruktorach
                                    //nie kopiujemy wskaznika bo by dwa obiekty dzielily jedna tablice!
                                    //nie chcemy tego, a jakby chcielismy to by uzylismy "statica", a nie kopiowania na chama
    for(int i=0;i<this->_dlugosc;i++)
    {
        this->_wek[i]=inny._wek[i]; // kopiujemy wartosci z tablicy jednego obiektu do drugiego
    }
}
VectorInt& VectorInt::operator= (const VectorInt& inny ) //operator przypisania
//dziala se podobnie jak konstruktor kopiujacy tylko tu juz mamy dwa utworzone obiekty wiec musimy pierwszemu obiektowi do
//ktorego kopiujemy wartosci drugiego se przerealokowac tablice, ale pozatym taka sama idea kopiowania jak wyzej
{
    if(this!=&inny)
    {
        delete this->_wek;
        this->_dlugosc=inny._dlugosc;
        this->_size=inny._size;
        this->_capacity=inny._capacity;
        this->_wek=new int[_dlugosc];
        for(int i=0;i<this->_dlugosc;i++)
        {
            this->_wek[i]=inny._wek[i];
        }

    }
    return *this;
}
VectorInt::~VectorInt()  //destruktor bozy, ktory czysci se pamiec, zeby zadnych wyciekow nie bylo i ogolem zeby z klasa sie
//obiekt zamykal xdddd. Ogolnie prawdziy obiekt poznajemy po tym jak konczy, a nie jak zaczyna. Podobnie jak mezczyzne.
//Czy to dlatego wiecej programistow jest plci meskiej? Nie wiem.
{
    _dlugosc=0;
    _size=0;
    _capacity=0;
    delete [] _wek; //czyscimy tablice
}
int VectorInt::at(int n) //wypisuje wartosc na n-tym miejscu - ogolnie ja indeksuje wektor od jedynki jak sie to robi w matmie
//dlatego czesto w tablicach sie -1 no bo tablize od zera
{
    return _wek[n-1];
}
void VectorInt::insert(int miejsce, int wartosc) //wpisuje na dane miejsce dana wartosc
{
    if (miejsce<=0) // chyba jasne
    {
        std::cout<<std::endl<<"Szanowny uzytkowniku oprogramowania - indeksujemy od 1!";
    }
    else if(miejsce>_dlugosc)//jak mamy miejsc na 5 elementow a uzytkownik chce cos na 7 postawic no to musimy pamiec realokować
                             //of course tylko do 7 bo po co wiecej
    {
        realok(miejsce);  //ta funkcja ktora jest nizej opisana realokuje tablice _wek[] na miejsce miejsc
                          //zachowuje oczywiscie przechowyane tam elementy, no chyba ze skracamy no to nie
        _wek[miejsce-1]=wartosc; //dopisujemy na miejsce-ym miejscu dana wartosc
    }
    else //jek sie miesci w pamieci
    {
        _wek[miejsce-1]=wartosc;  //se wpisujemy na miejsce-ym miejscu wartosc
    }
    reg_size(); //to nam ladnie ustala rozmiar wektora po zabawach z wpisywaniem
}
void VectorInt::pushback(int n)  //dodawanie kolejnego elementu
{
    if(_capacity==0)  //jesli nie ma wolnego miejsca, no to trza je powiekszyc, takie troche klasowe Liebensraum
    {
        realok(_dlugosc+1);  //powiekszamy se tablice o jeden bo po co wiecej
        _wek[_dlugosc-1]=n;      //dodajemy wartosc n na ostatnie dostepne miejsce
    }
    else  //jesli jest miejsce
    {
        _wek[_size]=n;  //no to wpisz na _size+1 miejscu (_size+1-1=_size)
    }
    reg_size();  //wiadomo
}
void VectorInt::popback() //usuwa se ostatni element wektora
{
    if(_size>0)
    {
        _wek[_size-1]=0; //zeruje _size miejsce
        reg_size(); //wiadomo
    }
    else //cout mowi za mnie
    {
        std::cout<<std::endl<<"Nie mozna skrocic zerowego wektora byku!";
    }
}
void VectorInt::shrinkToFit() //to se realokuje pamiec na tyle na ile wielki jest wektor
//jak mamy miejsca na 7 a nasz wektor to [1,2,3,4,5] to[1,2,3,4,5].shrinkToFit() se daje miejsca tylko na 5 bo po co wiecej
{
    reg_size(); //wiadomo
    realok(_size);  //realokuje pamiec tylka na rozm. wektora
    _capacity=0;  //nie ma wolnych miejsc bo tylko pamiec zajmuja
}
void VectorInt::clear() //se czysci wszystkie elementy i sprawia ze wektor jest pusty
{
    for(int i=0;i<_dlugosc;i++)
    {
        _wek[i]=0;  //cyk zerowanko
    }
    reg_size(); //wiadomo
}
int VectorInt::size() //tu wiadomo te 3
{
    return _size;
}
int VectorInt::capacity()
{
    return _capacity;
}
int VectorInt::get_dlugosc()
{
    return _dlugosc;
}
void VectorInt::realok(int n) // to se realokuje pamiec na n
{
    int tab[n]; //tworzy tablice pomocnicza o dlugosci jaka chcemy ze nasz _wek mial
    for(int i=0;i<n;i++)
    {
        if(i<_dlugosc)
        {
            tab[i]=_wek[i]; // tu kopiuje wszystkie elementy naszego starego wektora
        }
        else
        {
            tab[i]=0; //no a reszta cyk zerami wypelniamy
        }
    }
    delete [] _wek; //tera se wyczyscic mozemy nasz wektor
    _dlugosc=n; //zmienic liczba na jako alokujemy pamiec
    _wek = new int [_dlugosc]; //i cyka na nia se alokujemy
    for(int i=0;i<_dlugosc;i++)
    {
        _wek[i]=tab[i];  //i znowu kopiujemy i mamy znowu nasz wektor ze starymi wartosciami ale w innym rozmiarze
    }
}
void VectorInt::reg_size()  //to se reguluje size i capacity
//chodzi tu ze sprawdzmy od konca czy jest cos co nie jest zerem, no i pierwsze co bedzie to jego indeks to nasz nowy rozmiar
//wektora. Przykładowo [x,y=0,z=0]=[x]=x; [1,0,2,0,3,0]=[1,0,2,0,3]; 1,2030400000=1,20304 (liczba w systemie pozycyjnym
// to w sumie troche wektor)
{
    bool pom=true;
    for(int i=_dlugosc-1;i>=0;i--)
    {
        if(_wek[i]!=0)
        {
            _size=i+1;
            pom=false;
            break;
        }
    }
    if(pom) _size=0;
    _capacity=_dlugosc-_size;
}

//-----------------------------------------------------------------------------------

namespace patch // to z internetu, gdyz std::to_string nie chialo dzialac i na forum znalazlem rozwiazanie tego problemu
//przyznaje ze srednio wiem jak to dziala, no ale jest to latka zeby na moim komplatorze to_string dzialal
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

std::ostream& operator<<(std::ostream& out,VectorInt& wek)  //operator "<<" dla obiektow naszej klasy
//chodzi o to ze cout<<f; dla f dowolnego typu czy to int, float, char, string jest juz zdefiniowane
//no a ze my tworzymy wlasna klase to musimy tez ustalic jak cout ma sie zachowywac z elelmentami naszej klasy
{
    std::string ds="[ "; //tworzymy se strinda gdzie calego wektorka w ladnych prostokatnych nawiasach zapiszemy
    for(int i =1;i<wek.size();i++) //chcemy wypisac tylko elementy naszego wektora a nie zaalokowanej tablicy
    {
        ds=ds+ patch::to_string(wek.at(i))+", "; //dopisujemy se do niego wszystkie wartosci a to_string nam konwertuje
                                                  // int na stringa
    }
    ds=ds+ patch::to_string(wek.at(wek.size()))+" ]";
    return out<<ds; //zwracamy naszego stringa
                    //znaczy to ze jak mamy VectorInt w1 i zrobimy cout<<w1 to kompilatora to zinterpretuje jako:
                    //1.zbierz se wszystkie elementy w1 w stringa ds
                    //2.cout<<ds
                    //Czyli cout<<w1 == cout<<ds
}
