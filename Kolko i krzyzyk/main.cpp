#include <iostream>
#include <windows.h>
#include <conio.h>

using namespace std;
enum pole{PUSTE,KRZYZYK,KOLKO};
void wypisz_wiersz(char znak[3],int miejsce)
{
   cout<<"| ";
    for(int i=0;i<3;i++)
    {

            cout<<znak[i];
        cout<<" | ";
    }
}
void wypisz(char znak[3][3])
{
    cout<<"-------------"<<endl;
    wypisz_wiersz(znak[0],0);
    cout<<endl;
    cout<<"-------------"<<endl;
     wypisz_wiersz(znak[1],1);
    cout<<endl;
    cout<<"-------------"<<endl;
     wypisz_wiersz(znak[2],2);
    cout<<endl;
    cout<<"-------------"<<endl;

}
bool kontrola(int numer,char tab[3][3])
{
    if((numer<1)||(numer>9))
    {
        cout<<endl<<"Bledny numer - powtorz"<<endl;
        return false;
    }


    if(('X'==tab[numer/3][numer%3-1])||('O'==tab[numer/3][numer%3-1])) return false;
    else return true;


}
void wpisz(int numer,char tab[3][3],char co, pole tab2[3][3],pole dane)
{
   tab[numer/3][numer%3-1]= co;
   tab2[numer/3][numer%3-1]=dane;
}
bool sprawdzanko(pole tab[3][3])
{
    if((tab[0][0]==KRZYZYK&&tab[0][1]==KRZYZYK&&tab[0][2]==KRZYZYK)||
       (tab[1][0]==KRZYZYK&&tab[1][1]==KRZYZYK&&tab[1][2]==KRZYZYK)||
       (tab[2][0]==KRZYZYK&&tab[2][1]==KRZYZYK&&tab[2][2]==KRZYZYK)||
       (tab[0][0]==KRZYZYK&&tab[1][0]==KRZYZYK&&tab[2][0]==KRZYZYK)||
       (tab[0][1]==KRZYZYK&&tab[1][1]==KRZYZYK&&tab[2][1]==KRZYZYK)||
       (tab[0][2]==KRZYZYK&&tab[1][2]==KRZYZYK&&tab[2][2]==KRZYZYK)||
       (tab[0][0]==KRZYZYK&&tab[1][1]==KRZYZYK&&tab[2][2]==KRZYZYK)||
       (tab[0][2]==KRZYZYK&&tab[1][1]==KRZYZYK&&tab[2][0]==KRZYZYK))
    {
        cout<<endl<<endl<<"Wygral Krzyzyk!";
        return false;
    }
    else if((tab[0][0]==KOLKO&&tab[0][1]==KOLKO&&tab[0][2]==KOLKO)||
       (tab[1][0]==KOLKO&&tab[1][1]==KOLKO&&tab[1][2]==KOLKO)||
       (tab[2][0]==KOLKO&&tab[2][1]==KOLKO&&tab[2][2]==KOLKO)||
       (tab[0][0]==KOLKO&&tab[1][0]==KOLKO&&tab[2][0]==KOLKO)||
       (tab[0][1]==KOLKO&&tab[1][1]==KOLKO&&tab[2][1]==KOLKO)||
       (tab[0][2]==KOLKO&&tab[1][2]==KOLKO&&tab[2][2]==KOLKO)||
       (tab[0][0]==KOLKO&&tab[1][1]==KOLKO&&tab[2][2]==KOLKO)||
       (tab[0][2]==KOLKO&&tab[1][1]==KOLKO&&tab[2][0]==KOLKO))
    {
        cout<<endl<<endl<<"Wygralo Kolko!";
        return false;
    }
    else if(tab[0][0]!=PUSTE&&tab[0][1]!=PUSTE&&tab[0][2]!=PUSTE&&
            tab[1][0]!=PUSTE&&tab[1][1]!=PUSTE&&tab[1][2]!=PUSTE&&
            tab[2][0]!=PUSTE&&tab[2][1]!=PUSTE&&tab[2][2]!=PUSTE)
    {
       cout<<endl<<endl<<"Remis!";
        return false;
    }





    return true;
}


int main()
{
    int miejsce;
    bool gra=true;
    bool kol_krzyz=true;
    char znaki[3][3]={{'1','2','3'},{'4','5','6'},{'7','8','9'}};
    pole polka[3][3]={{PUSTE,PUSTE,PUSTE},{PUSTE,PUSTE,PUSTE},{PUSTE,PUSTE,PUSTE}};

    wypisz(znaki);
    while(gra)
    {
        if(kol_krzyz)
        {
          cout<<"Kolko: ";
        if(!(cin>>miejsce))
        {
            cout<<"Liczba!";
            break;
        }
        else
        {
           if(kontrola(miejsce,znaki))
            {
            wpisz(miejsce,znaki,'O',polka,KOLKO);
             }
            else continue;
        }


        kol_krzyz=false;
        }
        else
        {
         cout<<"Krzyzyk: ";
        if(!(cin>>miejsce))
        {
            cout<<"Liczba!";
            break;
        }

        if(kontrola(miejsce,znaki))
        {
            wpisz(miejsce,znaki,'X',polka,KRZYZYK);
        }
        else continue;
        kol_krzyz=true;
        }
        system("cls");

        wypisz(znaki);
        gra=sprawdzanko(polka);
    }
    getch();
    getch();


    return 0;
}
